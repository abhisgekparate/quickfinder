<?php
class Dashboard_model extends CI_Model {

    public function __construct() 
	{
        parent::__construct();
		
    }
	public function getTotalVisits()
	{
		$id = $this->session->userdata('userid');
		$count=$this->db->select('user_clicks')->where('id',$id)->get('stores')->result();
		if(empty($count))
			return  0;
		else
			return $count['user_clicks'];
	}
	public function getTotalStores()
	{
		$userid=$this->session->userdata('userid');
		$this->db->select('stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		$count= $this->db->where('stores.userid',$userid)->get()->num_rows();
		if(empty($count))
			return 0;
		else
			return $count;
	}
	public function getTotalStores1()
	{
		$id = $this->session->userdata('userid');
		$count=$this->db->select('*')->where('id',$id)->get('stores')->num_rows();
		if(empty($count))
			return 0;
		else
			return $count;
	}
}
?>