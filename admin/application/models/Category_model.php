<?php
class Category_model extends CI_Model
{
	
	public function addNewCategory()
	{
		$date = date_create();
		$data=array(
		'name'=>$this->usertype=$_POST['name'],
		'createdat'=> date_timestamp_get($date),
		'updatedat'=> date_timestamp_get($date),
		);
		return $this->db->insert('categories',$data);
	}
	public function getSubCategory($id)
	{
		return $this->db->select('*')->where('id',$id)->get('categories')->row();
		
	}
	public function addSubCategory()
	{
		$id=$_POST['id'];
		$subcategory=$_POST['subcategory'];
		$a=explode(',',$subcategory);
		for($i=0;count($a)>$i;$i++)
		{
			$data=array(
			'category_id'=>$id,
			'name'=>$a[$i],
			);
			$this->db->insert('subcategories',$data);
		}
		return true;
	}
	public function getCategory()
	{
		$categories = $this->db->get('categories')->result_array();
		
		$final_array = array();
		
		foreach($categories as $category)
		{
			$category_id = $category['id'];
			
			$query = $this->db->select('*')
			     ->from('subcategories')
		  	     ->where('category_id', $category_id)
			     ->get();
				 
			//print_r($query->result_array());
			//die();
			
			array_push($final_array,array(
			  'category'=>$category,
			  'subcategories'=>$query->result_array()
			));
		}
		
		return $final_array;
		
	}
	
}
?>