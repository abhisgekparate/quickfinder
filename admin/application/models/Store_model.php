<?php

class Store_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $id = $this->session->userdata('userid');
    }

    public function addNewStore($filename, $uploadData) {
    	/*$tags =$_POST['tags'];
       $addtag='';
       for ($i=0;count($tags)>$i;$i++) {
       $addtag .= " ".$tags[$i];
       }*/
	  
		//create store code
        $date = date_create();
        $data = array(
            'userid' => $this->session->userdata('userid'),
            'categoryid' => $this->categoryid = $_POST['categoryid'],
            'name' => $this->name = $_POST['name'],
            'email' => $this->email = $_POST['email'],
            'mobile' => $this->mobile = $_POST['mobile'],
			'country'=>$this->country = $_POST['country'],
			'state'=>$this->country = $_POST['state'],
			'city'=>$this->country = $_POST['city'],
			'area'=>$this->country = $_POST['area'],
            'description' => $this->description = $_POST['description'],
			//'tags'=>$addtag,
            'firstimage' => $this->firstimage = $filename,
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
        );
        $this->db->insert('stores', $data);
        $id = $this->db->insert_id();
		if($this->session->userdata('plan')==1)
				$this->entryPlan(1,$id);
		else
				$this->entryPlanWithoutPay(1,$id);	
        
		//multiple file upload code here
        if ($id) {
            if ($uploadData != '') {
                for ($j = 0; $j < count($uploadData); $j++) {
                    $data1 = array(
                        'images' => $this->images = $uploadData[$j]['file_name'],
                        'storeid' => $this->storeid = $id,
                        'createdat' => date('Y-m-d H:m:s'),
                        'updatedat' => date('Y-m-d H:m:s'),
                    );
                    $this->db->insert('stroe_images', $data1);
                }
                return TRUE;
            } else {
                return TRUE;
            }
        }
    }
	 public function addNewStore1($filename,$bannerfile, $uploadData) {
    	$tags =$_POST['tags'][0];
       $multipletag = explode(",", $tags);
       $addtag='';
       for ($i=0;count($multipletag)>$i;$i++) {
       $addtag .= " ".$multipletag[$i];
       }
		//create store code
        $date = date_create();
        $data = array(
            'userid' => $this->session->userdata('userid'),
            'categoryid' => $this->categoryid = $_POST['categoryid'],
            'name' => $this->name = $_POST['name'],
            'email' => $this->email = $_POST['email'],
            'mobile' => $this->mobile = $_POST['mobile'],
			'country'=>$this->country = $_POST['country'],
			'state'=>$this->country = $_POST['state'],
			'city'=>$this->country = $_POST['city'],
			'area'=>$this->country = $_POST['area'],
            'description' => $this->description = $_POST['description'],
			//'tags'=>$addtag,
            'firstimage' => $this->firstimage = $filename,
            'createdat' => date('Y-m-d H:m:s'),
            'updatedat' => date('Y-m-d H:m:s'),
        );
        $this->db->insert('stores', $data);
        $id = $this->db->insert_id();
		if($this->session->userdata('plan')==1)
				$this->entryPlan(1,$id);
		elseif($this->session->userdata('plan')==4)
				$this->platinumPlan(4,$id,$bannerfile);	
		else
				$this->entryPlanWithoutPay(1,$id);	
        
		//multiple file upload code here
        if ($id) {
            if ($uploadData != '') {
                for ($j = 0; $j < count($uploadData); $j++) {
                    $data1 = array(
                        'images' => $this->images = $uploadData[$j]['file_name'],
                        'storeid' => $this->storeid = $id,
                        'createdat' => date('Y-m-d H:m:s'),
                        'updatedat' =>date('Y-m-d H:m:s'),
                    );
                    $this->db->insert('stroe_images', $data1);
                }
                return TRUE;
            } else {
                return TRUE;
            }
        }
    }
	public function platinumPlan($plan,$storeid,$bannerfile)
	{	
		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		
		$data=array(
					'storeid'=>$storeid,
					'paymentid'=>$plan,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>1,
					'Paid'=>1,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
		$bannerData=array('storeid'=>$storeid,'images'=>$bannerfile,'IsActive'=>1);
		
		$this->db->insert('banner', $bannerData);
		$this->session->unset_userdata('plan');
		return $this->db->insert('store_payment', $data);
		
	}
	public function platinumPlanB($plan,$storeid,$bannerfile)
	{	

		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		
		$data=array(
					'paymentid'=>$plan,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>1,
					'Paid'=>1,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
	
		$bannerData=array('storeid'=>$storeid,'images'=>$bannerfile,'IsActive'=>1);
		$this->db->insert('banner', $bannerData);
		$this->session->unset_userdata('plan');
		return $this->db->where('storeid',$storeid)->update('store_payment', $data);
	}
	public function entryPlan($plan,$storeid)
	{
		
		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		
		$data=array(
					'storeid'=>$storeid,
					'paymentid'=>$plan,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>1,
					'Paid'=>1,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
		$this->session->unset_userdata('plan');
		return $this->db->insert('store_payment', $data);
	}
	public function entryPlanWithoutPay($plan,$storeid)
	{
		
		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		$data=array(
					'storeid'=>$storeid,
					'paymentid'=>$plan,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>0,
					'Paid'=>0,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
		$this->session->unset_userdata('plan');
		return $this->db->insert('store_payment', $data);
	}
	public function getStoreInfo()
	{
		return $this->db->get('stores')->result_array();
	}
	public function getStoreData($id)
	{
		return $this->db->where('id',$id)->get('stores')->row();
	}
	public function getMenu()
	{
		return $this->db->get('categories')->result_array();
	}
	public function getStoreImages($id)
	{
		return $this->db->where('storeid',$id)->get('stroe_images')->result_array();
	}
	public function getStoresInfoByCategory($id)
	{
		if(($this->db->where('categoryid',$id)->get('stores')->num_rows()) > 0)
		{
			
			return $this->db->where('categoryid',$id)->get('stores')->result_array();
		}
		else {
			return "no recode found !!!";
		}
	}
	public function getCities($state_id)
	{

		return  $this->db->where('state_id',$state_id)->get('cities')->result_array();
	}
	public function getStateData($country_id)
	{
		
		   return  $this->db->where('country_id',$country_id)->get('states')->result_array();
		   
		   // return $statedata->;
	}
	public function getCountry()
	{
		return $this->db->get('countries')->result_array();
	}
	public function get_mysqli()
	{
		$db = (array)get_instance()->db;
		return mysqli_connect('localhost', $db['username'], $db['password'], $db['database']);
	}
	public function getStoresInfoBySearchBox($city,$search,$area)
	{
		$query=explode(" ",$search);
		$records=array();
			if(!empty($city) && !empty($query) && !empty($area))
			{
				return $this->ifNotNullSearch($city,$query,$area);
			}
			elseif(empty($city) && !empty($query) && !empty($area))
			{
				return $this->ifCityNull($query,$area);
			}
			elseif(empty($city) && !empty($query) && empty($area))
			{
				
				return $this->ifAreaNull($query);
			}
			elseif(!empty($city) && !empty($query) && empty($area))
			{
				return $this->ifAreaNull1($city,$query);
			}
	}
	public function ifAreaNull1($city,$query)
	{
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "tags LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="select * from stores where payment_id=3 and is_varified='1' and city='$city' and  ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where payment_id=2 and is_varified='1' and city='$city' and  ( $condition)";
				$standardRecords=$this->db->query($sql)->result_array();
		
				$sql="select * from stores where  payment_id = 1  and is_varified='1'  and city='$city'  and ( $condition)";
				$basicRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where  payment_id in (1,2,3)  and is_varified='0'  and city='$city'  and ( $condition)";
				$normalRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				foreach($standardRecords as $row )
				{
					$records[]=$row;
				}
				foreach($basicRecords as $row)
				{
					$records[]=$row;
				}
				foreach($normalRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function ifAreaNull($query)
	{
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "tags LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="select * from stores where payment_id=3 and is_varified='1'   and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where payment_id=2 and is_varified='1'  and  ( $condition)";
				$standardRecords=$this->db->query($sql)->result_array();
		
				$sql="select * from stores where  payment_id = 1  and is_varified='1'  and ( $condition)";
				$basicRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where  payment_id in (1,2,3)  and is_varified='0'  and ( $condition)";
				$normalRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				foreach($standardRecords as $row )
				{
					$records[]=$row;
				}
				foreach($basicRecords as $row)
				{
					$records[]=$row;
				}
				foreach($normalRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function ifNotNullSearch($city,$query,$area)
	{
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "tags LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="select * from stores where payment_id=3 and is_varified='1' and city='$city' and area='$area' and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where payment_id=2 and is_varified='1' and city='$city' and area='$area' and ( $condition)";
				$standardRecords=$this->db->query($sql)->result_array();
		
				$sql="select * from stores where  payment_id = 1  and is_varified='1'  and city='$city' and area='$area'  and ( $condition)";
				$basicRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where  payment_id in (1,2,3)  and is_varified='0'  and city='$city' and area='$area'   and ( $condition)";
				$normalRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				foreach($standardRecords as $row )
				{
					$records[]=$row;
				}
				foreach($basicRecords as $row)
				{
					$records[]=$row;
				}
				foreach($normalRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function ifCityNull($query,$area)
	{
				$condition='';
				for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "tags LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="select * from stores where payment_id=3 and is_varified='1' and  area='$area' and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where payment_id=2 and is_varified='1'  and area='$area' and ( $condition)";
				$standardRecords=$this->db->query($sql)->result_array();
		
				$sql="select * from stores where  payment_id = 1  and is_varified='1'  and area='$area' and ( $condition)";
				$basicRecords=$this->db->query($sql)->result_array();
				
				$sql="select * from stores where  payment_id in (1,2,3)  and is_varified='0'  and area='$area' and ( $condition)";
				$normalRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				foreach($standardRecords as $row )
				{
					$records[]=$row;
				}
				foreach($basicRecords as $row)
				{
					$records[]=$row;
				}
				foreach($normalRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function getNumStore()
	{
		return  $this->db->where('userid',$this->session->userdata('userid'))->get('stores')->num_rows();
	}
    public function getStores(){
		$userid=$this->session->userdata('userid');
		$this->db->select('stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		return $this->db->where('stores.userid',$userid)->get()->result_array();	
	}
	public function updateUserView($id,$viewCount)
	{
		$totalview=$this->db->select('user_clicks')->where('id',$id)->get('stores')->row();
		$count=$totalview->user_clicks + $viewCount;
		$data=array(
		'user_clicks'=>$count,
		);
		$this->db->where('id', $id);
		return $this->db->update('stores', $data);
	}
	public function paymentPay($id,$store_id)
	{
		
		$occDate=date('Y-m-d');
		$forOdNextMonth= date('m', strtotime("+1 month", strtotime($occDate)));
		$nextDate=date('Y-'.$forOdNextMonth.'-d');
		$data=array(
					'paymentid'=>$id,
					'startdate'=>date('Y-m-d'),
					'enddate'=>$nextDate,
					'IsActive'=>1,
					'Paid'=>1,
					'timestamp'=>date('Y-m-d H:m:s'),
		);
		$this->session->unset_userdata('store_id');
		return $this->db->where('storeid',$store_id)->update('store_payment', $data);
	}
	public function getCalenderData()
	{
		$this->db->select('*');
		$this->db->from('calender');
		$this->db->join('month','month.id=calender.monthID');
		return $this->db->where('calender.IsActive','1')->get()->result_array();
	}

}

?>