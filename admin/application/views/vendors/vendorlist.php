<!-- start: Content -->
<style>
img {
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
    width: 150px;
}

img:hover {
    box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
</style>
<div id="content">
    <div class="panel box-shadow-none content-header">
        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Vendors</h3>
                <p class="animated fadeInDown">
                    Vendors <span class="fa-angle-right fa"></span> List Vendors
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    </div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                
                                    <th>Store Name</th>
                                    <th>Images</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
									<th>Payment Type</th>
									<th>Plan Duration</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
								
                                foreach ($venders as $row) {
                                    $i++;
                                   
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
										<td><?php echo $row['data']['name']; ?></td>
                                        <td>
                                            <img src="<?php echo base_url().$row['data']['firstimage']; ?>" style="width:150px;height:100px">
                                        </td>
										<td><?php echo $row['data']['email']; ?></td>
                                        <td><?php echo $row['data']['mobile']; ?></td>
										<td><?php echo $row['payment_type']; ?></td>
										<td><?php echo $row['data']['startdate'].' To '.$row['data']['enddate']; ?></td>
                                    </tr>
                                <?php }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<!-- end: content -->