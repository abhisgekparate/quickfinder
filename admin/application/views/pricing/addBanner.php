


<div id="content">
    <div class="panel box-shadow-none content-header">
        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Store</h3>
                <p class="animated fadeInDown">
                   <a href='<?php echo site_url('store');?>'>Store List</a> <span class="fa-angle-right fa"></span> Add New Store
                </p>
            </div>
        </div>
    </div>
    <div class="form-element">
        <div class="col-md-12">
            <div class="col-md-12 panel">
                <div class="col-md-12 panel-heading">
                    <h4>
						<?php
								if($this->session->userdata('plan')==1) 
									echo 'Entry Plan:';
								elseif($this->session->userdata('plan')==2) 
									echo 'Silver Plan:';
								elseif($this->session->userdata('plan')==3) 
									echo 'Gold Plan:';
								elseif($this->session->userdata('plan')==4) 
									echo 'Platinum Plan:';			
						?>
						Add New Store
					</h4>
                </div>

                <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                    <div class="col-md-12">
                        <form class="cmxform" id="signupForm" method="post" action="<?php echo site_url('PlatinumPlanB'); ?>" enctype="multipart/form-data">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="file" class="form-text" id="img"   name="banner"/>
                                    <span class="bar"></span>
                                    <label>Banner Image</label>
                                </div>
								
                                

                                <div class="row" id="image_preview"></div>
                                <div class="col-md-12">

                                    <input class="submit btn  btn-gradient btn-info" type="submit" value="Submit">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




