<!-- start: Content -->
<div id="content">
    <div class="panel box-shadow-none content-header">
        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">payment List</h3>
                <p class="animated fadeInDown">
                    payment <span class="fa-angle-right fa"></span> payment List
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                   </div>
                <div class="panel-body">

                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>payment Type</th>
                                    <th>Amount </th>
									 <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (is_array($payment)) {
                                    $i = 0;
                                    foreach ($payment as $row) {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $row['payment_type']; ?></td>
											<td><?php echo $row['amt']; ?></td>
                                            <td>  <a href="<?php echo site_url('addSubCategory'); ?>">
                           <button class="btn btn-primary" >
                                <span>Subsribe</span>
                            </button> </a>
							<a href="<?php echo site_url('paymentdetails/'.$row['id']); ?>">
							<button class="btn btn-info" >
                                <span>Details</span>
                            </button> 							
                            </a>
                        </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td>NO Record Found !...</td>

                                    </tr>
                                    <?php
                                }
                                ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<!-- end: content -->