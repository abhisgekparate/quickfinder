<!-- start: content -->
            <div id="content">
               <div class="col-md-12" style="padding:20px;">
                    <div class="col-md-12 padding-0">
                        <div class="col-md-8 padding-0">
                            <div class="col-md-12 padding-0">
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                                          <h4 class="text-left">User Visit</h4>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-user icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <h1><?php echo $visits;?></h1>
                                        <p></p>
                                        <hr/>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                                          <h4 class="text-left">Total Stores</h4>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icons icon-film text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <h1><?php echo $stores;?></h1>
                                        
                                        <hr/>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12 padding-0">
                              <div class="panel box-v2">
                                  <div class="panel-heading padding-0">
                                    <img src="asset/img/bg2.jpg" class="box-v2-cover img-responsive"/>
                                    <div class="box-v2-detail">
                                      <img src="asset/img/avatar.jpg" class="img-responsive"/>
                                      <h4><?php echo $this->session->userdata('email');?></h4>
                                    </div>
                                  </div>
							</div>
                            </div>
                        </div>
						<div class="col-sm-6 col-md-3 price padding-0">
                      <div class="panel">
					  <div class="panel-header text-white text-center bg-dark-deep-purple">
                            <b>Add New Store</b>
                        </div>
                         <div class="badges-ribbon">
                          <div class="badges-ribbon-content badge-success">Free</div>
                        </div>
                        <div class="panel-header text-white bg-blue price-money text-center">
                          <span>
                              <sup>&#x20B1;</sup>99<sub>/month</sub>
                              </span>
                        </div>
                         <ul class="list-group text-center">
                            <li class="list-group-item">
                              Listing Free
                              <i class="fa fa-check text-success"></i>
                            </li>
							 <li class="list-group-item">
                              Full Access
                              <i class="fa fa-check text-success"></i>
                            </li>
                              <li class="list-group-item">
                              Documentation
                              <i class="fa fa-check text-success"></i>
                            </li>
							 <li class="list-group-item">
                               Free Update
                              <i class="fa fa-check text-success"></i>
                            </li>
                            <li class="list-group-item" style="padding:20px;">
                            <center>
							
							<a href="<?php echo site_url('EntryPlan');  ?>">
                              <input type="button" class="btn box-shadow-none text-white bg-indigo" value="Subscribe"/>
                              </a>
							  </center>
                            </li>
                          </ul>
                      </div>
                    </div>
                    </div>
                </div>
      		  </div>
          <!-- end: content -->