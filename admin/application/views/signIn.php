<!DOCTYPE html>
<html lang="en">
<head>

 <meta charset="utf-8">
 
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Quickfinder</title>

  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/plugins/simple-line-icons.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/plugins/animate.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/plugins/icheck/skins/flat/aero.css"/>
  <link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet">
  <!-- end: Css -->

  <link rel="shortcut icon" href="<?php echo base_url(); ?>asset/img/logomi.png">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

       <style type="text/css">
  .form-signin-wrapper {
    background: #fff !important;
}
.form-signin {
    max-width: 430px;
    padding: 10px;
    margin: 0 auto;
}
.error
{color:red;}
</style>
    </head>

    <body id="mimin" class="dashboard form-signin-wrapper">

      <div class="container" >

        <form action="<?php echo site_url('login');?>" class="form-signin" method="post">
          <div class="panel periodic-login" style="background: #47b4e2; box-shadow: 0 7px 16px #47b4e2, 0 4px 5px #47b4e2;    margin-top: 30px;">
              <!-- <span class="atomic-number">28</span> -->
              <div class="panel-body text-center">
               <h1 class="" style="margin-top: 2px;">Quickfinder</h1>
                  <!-- <h1 class="atomic-symbol">Mi</h1>
                  <p class="atomic-mass">14.072110</p>
                  <p class="element-name">Miminium</p> -->

                  <i class="icons icon-arrow-down"></i><p style="color: green;
    font-size: 15px;
    font-weight: bold">
				  <?php 
				  echo $this->session->userdata('msg');
				 $this->session->unset_userdata('msg');
				  ?></p>
				  <p style="color:red;
    font-size: 15px;
    font-weight: bold">
				  <?php 
				 
				   echo $this->session->userdata('errmsg');
				 $this->session->unset_userdata('errmsg');
				  ?></p>
                  <div class="form-group form-animate-text" style="margin-top:30px !important;">
                    <input type="text" class="form-text" name="username" required>
                    <span class="bar"><?php echo form_error('username', '<div class="error">', '</div>'); ?></span>
                    <label>Username</label>
                  </div>
                  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                    <input type="password" class="form-text" name="password" required>
                    <span class="bar"><?php echo form_error('password', '<div class="error">', '</div>'); ?></span>
                    <label>Password</label>
                  </div>
                  
                  <input type="submit" class="btn col-md-12" value="SignIn"/>
              </div>
                <div class="text-center" style="padding:5px;">
                    <a href="<?php echo site_url('signup');?>">Create New Acoount</a>
                </div>
          </div>
        </form>

      </div>

      <!-- end: Content -->
      <!-- start: Javascript -->
       <script src="<?php echo base_url(); ?>asset/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>asset/js/jquery.ui.min.js"></script>
      <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>

      <script src="<?php echo base_url(); ?>asset/js/plugins/moment.min.js"></script>
      <script src="<?php echo base_url(); ?>asset/js/plugins/icheck.min.js"></script>

      <!-- custom -->
      <script src="<?php echo base_url(); ?>asset/js/main.js"></script>
      <script type="text/javascript">
       $(document).ready(function(){
         $('input').iCheck({
          checkboxClass: 'icheckbox_flat-aero',
          radioClass: 'iradio_flat-aero'
        });
       });
     </script>
     <!-- end: Javascript -->
   </body>
   </html>