		<div id="content">
                <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Category</h3>
                        <p class="animated fadeInDown">
                          Form <span class="fa-angle-right fa"></span> Add New Category
                        </p>
                    </div>
                  </div>
                </div>
                <div class="form-element">
				<div class="col-md-12">
                  <div class="col-md-12 panel">
                    <div class="col-md-12 panel-heading">
                      <h4>Add New Category Form</h4>
                    </div>
                    <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                      <div class="col-md-12">
                        <form class="cmxform" id="signupForm" method="post" action="<?php echo site_url('addNewCategory');?>">
                          <div class="col-md-6">
                            <div class="form-group form-animate-text" style="margin-top:40px !important;">
                              <input type="text" class="form-text" id="validate_firstname" name="name" required>
                              <span class="bar"></span>
                              <label>Category Name</label>
                            </div>

                            
                          </div>                   
                          <div class="col-md-12">
                              <div class="form-group form-animate-checkbox">
                                  <input type="checkbox" class="checkbox"  id="validate_agree" name="validate_agree">
                                  <label>Please agree to our policy</label>
                              </div>
                              <input class="submit btn btn-danger" type="submit" value="Submit">
                        </div>
                      </form>

                    </div>
                  </div>
                </div>
              </div>
			  </div>
			  </div>