<!-- start: Content -->
 
  
<div id="content">
    <div class="panel box-shadow-none content-header">
        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Category List</h3>
                <p class="animated fadeInDown">
                    Category <span class="fa-angle-right fa"></span> Category List
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3>
                        <a href="<?php echo site_url('addCategory'); ?>">
                            <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add Category</span>
                            </button>
                            
                        </a>	

                         	  </h3></div>
                <div class="panel-body">

                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Category Name</th>
                                    <th>Subcategories</th>
                                    <th>Operations </th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--<a href="<?php echo site_url('addSubCategory/'.$row['category']['id']); ?>"></a>-->
								<?php
								
                                if (is_array($categorylist)) {
                                    $i = 0;
                                    foreach ($categorylist as $row) {
										
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $row['category']['name']; ?></td>
											
											<td><?php
											
											$sub_cats = Array();
											foreach($row['subcategories'] as $u) $sub_cats[] = $u['name'];
											
											$str = implode(",",$sub_cats);
											echo $str;
											?></td>
											
                                            <td>  
											<a href="<?php echo site_url('addSubCategory/'.$row['category']['id']); ?>">
                           <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add SubCategory</span>
                            </button>
						</a>							
                            
                        </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td>NO Record Found !...</td>

                                    </tr>
                                    <?php
                                }
                                ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<!-- end: content -->
