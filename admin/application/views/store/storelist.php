<!-- start: Content -->
<style>
img {
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
    width: 150px;
}

img:hover {
    box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
</style>
<div id="content">
    <div class="panel box-shadow-none content-header">
        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Store</h3>
                <p class="animated fadeInDown">
                    Store <span class="fa-angle-right fa"></span> List Store
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3>
					<?php if($this->session->userdata('num_store')){?>
                            <!--<a href="<?php echo site_url('addstore');  ?>">
						
                            <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add Store</span>
                            </button>
                        </a>-->
								
					<?php }else{
						?>
						<a href="<?php echo site_url('pricing');  ?>">
                            <button class="btn ripple btn-gradient btn-info" style="width:150px">
                                <span>Add Store</span>
                            </button>
                        </a> 
						<?php
					} ?>		</h3></div>
                <div class="panel-body">
                    <div class="responsive-table">
                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <!--<th>Sr.No</th>-->
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
									<th>Payment Type</th>
									<th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($storelist as $row) {
                                    $i++;
                                    //print_r($row);
                                    //print_r($row['data']['storedata']);
                                    //echo $row['data']['storedata']['id'];
                                    ?>
                                    <tr>
                                        <!--<td><?php echo $i; ?></td>-->
                                        <td>
                                            <img src="<?php echo base_url().$row['firstimage']; ?>" style="width: 202px;height: 140px">
                                        </td>
                                        <td><?php echo $row['storename']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['email']; ?></td>
                                        <td><?php echo $row['mobile']; ?></td>
                                        <td><span class="label label-primary"><?php echo $row['payment_type'];?></span></td>
										<td>
										<?php if($row['IsActive']==1){?>
										<input type="button" class="btn btn-success" value="Active"/>
										<?php }else{?>
										<a href="<?php echo site_url('pricingp/'.$row['id'])?>"><input type="button" class="btn btn-danger" value="UnActive"></a>
										<?php }?>
										</td>
                                    </tr>
                                <?php }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<!-- end: content -->