		
		<link rel="stylesheet" href="<?php echo base_url();?>dist/css/selectize.default.css">
		<script src="<?php echo  base_url();?>js/jquery.min.js"></script>
		<script src="<?php echo  base_url();?>js/selectize.js"></script>
		<script src="<?php echo  base_url();?>js/index.js"></script>
		
<div id="content">
            <div class="tabs-wrapper text-center">             
             <div class="panel box-shadow-none text-left content-header">
                  <div class="panel-body" style="padding-bottom:0px;">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Add Store</h3>
                        <p class="animated fadeInDown">
                        Plan <span class="fa-angle-right fa"></span> <?php
								if($this->session->userdata('plan')==1) 
									echo 'Entry Plan';
								elseif($this->session->userdata('plan')==2) 
									echo 'Silver Plan';
								elseif($this->session->userdata('plan')==3) 
									echo 'Gold Plan';
								elseif($this->session->userdata('plan')==4) 
									echo 'Platinum Plan';			
						?>
                        </p>
                    </div>
                   
                  </div>
              </div>
            <div class="col-md-12 tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="tabs-area-demo" aria-labelledby="tabs1">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <div class="col-md-12 tabs-area">
                      <div class="liner"></div>
                      <ul class="nav nav-tabs nav-tabs-v5" id="tabs-demo6">
                        <li class="active">
                         <a href="#tabs-demo6-area1" data-toggle="tab" title="welcome">
                          <span class="round-tabs one">
                            <i class="glyphicon glyphicon-home"></i>
                          </span> 
                        </a>
                      </li>

                      <li>
                        <a href="#tabs-demo6-area2" data-toggle="tab" title="profile">
                         <span class="round-tabs two">
                           <i class="glyphicon glyphicon-user"></i>
                         </span> 
                       </a>
                     </li>

                     <li>
                      <a href="#tabs-demo6-area3" data-toggle="tab" title="bootsnipp goodies">
                       <span class="round-tabs three">
                        <i class="glyphicon glyphicon-gift"></i>
                      </span> </a>
                    </li>

                    <li>
                      <a href="#tabs-demo6-area4" data-toggle="tab" title="blah blah">
                       <span class="round-tabs four">
                         <i class="glyphicon glyphicon-comment"></i>
                       </span> 
                     </a>
                   </li>

                   <li><a href="#tabs-demo6-area5" data-toggle="tab" title="completed">
                     <span class="round-tabs five">
                      <i class="glyphicon glyphicon-ok"></i>
                    </span> </a>
                  </li>
                </ul>
                <div class="tab-content tab-content-v5">
                  <div class="tab-pane fade in active" id="tabs-demo6-area1">

                    <div class="col-md-12">
					 <form class="cmxform" id="signupForm" method="post" action="<?php echo site_url('addnewstoreP')?>" enctype="multipart/form-data">
                     <div class="col-md-6">
                      <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" id="validate_firstname" name="name" required >
                                    <span class="bar"></span>
                                    <label>Store Name</label>
                      </div>

                       <div class="form-group form-animate-text" style="margin-top:40px !important;">

                                    <select class="form-text" name="categoryid">
                                        <option value="0">Select category</option>
                                        <?php
                                        print_r($categorylist);
                                        foreach ($categorylist as $row) {
                                            ?>
                                            <option value="<?php echo $row['category']['id']; ?>"><?php echo ucwords($row['category']['name']); ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                      <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="email" class="form-text" id="validate_firstname" name="email" required>
                                    <span class="bar"></span>
                                    <label>Store Email</label>
                                </div>

                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" minlength="10" maxlength="10" id="validate_firstname" name="mobile" required>
                                    <span class="bar"></span>
                                    <label>Store Mobile</label>
                                </div>
								<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <textarea  class="form-text" name="description" required></textarea>
                                    <!-- <input type="text" class="form-text" id="validate_firstname" name="name" required> -->
                                    <span class="bar"></span>
                                    <label>Store Descripion</label>
                                </div>
					  
                    </div>
                    <div class="col-md-6">
                      <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text country" name="country" >
                                        <option value="0">Select Country</option>
                                        <?php
                                        foreach ($country as $row) {
                                            ?>
                                            <option id="<?php echo $row['id']; ?>" value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
						<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text states" name="state" >
                                        <option value="0">Select State</option>
                                      <!--   <?php
                                        foreach ($state as $row) {
                                            ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?> -->
                                    </select>
                                </div>

                      <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text cities" name="city">
                                        <option value="0">Select city</option>
                                        <!-- <?php
                                        foreach ($city as $row) {
                                            ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?> -->
                                    </select>
                                </div>
								<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" id="validate_firstname" name="area" required >
                                    <span class="bar"></span>
                                    <label>Area</label>
                                </div>
						
                      
					 
                    </div>
						<div class="col-md-12">
                              
                              <a href="#tabs-demo6-area2" data-toggle="tab" id="next1"><input class="submit btn btn-danger" type="button" value="NEXT"></a>
                        </div>
						
                  </div>
                        
                           
				 </div>
				 
					
                  <div class="tab-pane fade" id="tabs-demo6-area2">
				 
						<label for="select-state">Keywords </label>
					<select id="select-state" multiple name="tags[]" class="demo-default" style="width:50%">
						<option value="">Select a state...</option>
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
						<option value="CA">California</option>
						<option value="CO">Colorado</option>
						<option value="CT">Connecticut</option>
						<option value="DE">Delaware</option>
						<option value="DC">District of Columbia</option>
						<option value="FL">Florida</option>
						<option value="GA">Georgia</option>
						<option value="HI">Hawaii</option>
						<option value="ID">Idaho</option>
						<option value="IL">Illinois</option>
						<option value="IN">Indiana</option>
						<option value="IA">Iowa</option>
						<option value="KS">Kansas</option>
						<option value="KY">Kentucky</option>
						<option value="LA">Louisiana</option>
						<option value="ME">Maine</option>
						<option value="MD">Maryland</option>
						<option value="MA">Massachusetts</option>
						<option value="MI">Michigan</option>
						<option value="MN">Minnesota</option>
						<option value="MS">Mississippi</option>
						<option value="MO">Missouri</option>
						<option value="MT">Montana</option>
						<option value="NE">Nebraska</option>
						<option value="NV">Nevada</option>
						<option value="NH">New Hampshire</option>
						<option value="NJ">New Jersey</option>
						<option value="NM">New Mexico</option>
						<option value="NY">New York</option>
						<option value="NC">North Carolina</option>
						<option value="ND">North Dakota</option>
						<option value="OH">Ohio</option>
						<option value="OK">Oklahoma</option>
						<option value="OR">Oregon</option>
						<option value="PA">Pennsylvania</option>
						<option value="RI">Rhode Island</option>
						<option value="SC">South Carolina</option>
						<option value="SD">South Dakota</option>
						<option value="TN">Tennessee</option>
						<option value="TX">Texas</option>
						<option value="UT">Utah</option>
						<option value="VT">Vermont</option>
						<option value="VA">Virginia</option>
						<option value="WA">Washington</option>
						<option value="WV">West Virginia</option>
						<option value="WI">Wisconsin</option>
						<option value="WY" selected>Wyoming</option>
					</select>
					<div class="col-md-12">
                              
                              <a href="#tabs-demo6-area3" data-toggle="tab" id="next2"><input class="submit btn btn-danger" type="button" value="NEXT"></a>
                        </div>
				<script>
				var eventHandler = function(name) {
				
					return function() {
						console.log(name, arguments);
						$('#log').append('<div><span class="name">' + name + '</span></div>');
					};
				};
				var $select = $('#select-state').selectize({
					create          : true,
					onChange        : eventHandler('onChange'),
					onItemAdd       : eventHandler('onItemAdd'),
					onItemRemove    : eventHandler('onItemRemove'),
					onOptionAdd     : eventHandler('onOptionAdd'),
					onOptionRemove  : eventHandler('onOptionRemove'),
					onDropdownOpen  : eventHandler('onDropdownOpen'),
					onDropdownClose : eventHandler('onDropdownClose'),
					onFocus         : eventHandler('onFocus'),
					onBlur          : eventHandler('onBlur'),
					onInitialize    : eventHandler('onInitialize'),
				});
				</script>
                  </div>
                  <div class="tab-pane fade" id="tabs-demo6-area3">
                   <div class="col-md-12">
                   <div class="col-md-6">
				   
				   <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="file" class="form-text" id="img"   name="img"/>
                          <span class="bar"></span>
                          <label>coverImage</label>
                    </div>
					</div>
					<div class="col-md-6">	
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="file" class="form-text" id="images" name="images[]" onchange="preview_images();" multiple/>
                                    <span class="bar"></span>
                                    <label>Store Multiple Image</label>
                                </div>
								<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="file" class="form-text" id="img"   name="banner"/>
                                    <span class="bar"></span>
                                    <label>Add Banner</label>
                                </div>
								</div>
                  </div>
				  <div class="col-md-12">
                              
                             <input class="submit btn btn-danger" type="submit" value="NEXT">
                        </div>
				  </div>
				  </form>
                  <div class="tab-pane fade" id="tabs-demo6-area4">
                    <h3 class="head text-center">Drop comments!</h3>
                    <p class="narrow text-center">
                      Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>

                    <p class="text-center">
                      <a href="" class="btn btn-success btn-round green"> start using Mimin <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                    </p>
                  </div>
                  <div class="tab-pane fade" id="tabs-demo6-area5">
                    <div class="text-center">
                      <i class="img-intro icon-checkmark-circle"></i>
                    </div>
                    <h3 class="head text-center">thanks for staying tuned! <span style="color:#f48250;">♥</span> Bootstrap</h3>
                    <p class="narrow text-center">
                      Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>      
              </div>
             
                
                
                </div>
                
              </div>
            </div>
          </div>