


<div id="content">
    <div class="panel box-shadow-none content-header">
        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Store</h3>
                <p class="animated fadeInDown">
                   <a href='<?php echo site_url('store');?>'>Store List</a> <span class="fa-angle-right fa"></span> Add New Store
                </p>
            </div>
        </div>
    </div>
    <div class="form-element">
        <div class="col-md-12">
            <div class="col-md-12 panel">
                <div class="col-md-12 panel-heading">
                    <h4>
						<?php
								if($this->session->userdata('plan')==1) 
									echo 'Entry Plan:';
								elseif($this->session->userdata('plan')==2) 
									echo 'Silver Plan:';
								elseif($this->session->userdata('plan')==3) 
									echo 'Gold Plan:';
								elseif($this->session->userdata('plan')==4) 
									echo 'Platinum Plan:';			
						?>
						Add New Store
					</h4>
                </div>

                <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                    <div class="col-md-12">
                        <form class="cmxform" id="signupForm" method="post" action="<?php echo site_url('addnewstoreP'); ?>" enctype="multipart/form-data">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" id="validate_firstname" name="name" required >
                                    <span class="bar"></span>
                                    <label>Store Name</label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">

                                    <select class="form-text" name="categoryid">
                                        <option value="0">Select category</option>
                                        <?php
                                        print_r($categorylist);
                                        foreach ($categorylist as $row) {
                                            ?>
                                            <option value="<?php echo $row['category']['id']; ?>"><?php echo ucwords($row['category']['name']); ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="email" class="form-text" id="validate_firstname" name="email" required>
                                    <span class="bar"></span>
                                    <label>Store Email</label>
                                </div>

                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" minlength="10" maxlength="10" id="validate_firstname" name="mobile" required>
                                    <span class="bar"></span>
                                    <label>Store Mobile</label>
                                </div>
                               



                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <textarea  class="form-text" name="description" required></textarea>
                                    <!-- <input type="text" class="form-text" id="validate_firstname" name="name" required> -->
                                    <span class="bar"></span>
                                    <label>Store Descripion</label>
                                </div>
								<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text country" name="country" >
                                        <option value="0">Select Country</option>
                                        <?php
                                        foreach ($country as $row) {
                                            ?>
                                            <option id="<?php echo $row['id']; ?>" value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div><div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text states" name="state" >
                                        <option value="0">Select State</option>
                                      <!--   <?php
                                        foreach ($state as $row) {
                                            ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?> -->
                                    </select>
                                </div><div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text cities" name="city">
                                        <option value="0">Select city</option>
                                        <!-- <?php
                                        foreach ($city as $row) {
                                            ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?> -->
                                    </select>
                                </div>
								 <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" id="validate_firstname" name="area" required >
                                    <span class="bar"></span>
                                    <label>Area</label>
                                </div>
								<div class="form-group form-animate-text" style="margin-top:40px !important;">


                            <div class="bs-example" >
            <input type="text" name="tags[]"   value="" data-role="tagsinput"  />
            
          </div>
                                     <!-- <ul id="tag" name="tags[]" class="form-text" ></ul> -->
                                <!--      <ul id="myTags" class="tagit ui-widget ui-widget-content ui-corner-all">
<li class="tagit-new">
<input type="text" class="ui-widget-content ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
<input type="hidden" style="display:none;" name="tages">

</li>
</ul> -->

                                    <!-- <input type="text" class="form-text" id="validate_firstname" name="tags" id="tag" required> -->
                                    <span class="bar"></span>
                                    <label>Store Tag</label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="file" class="form-text" id="img"  required name="img"/>
                                    <span class="bar"></span>
                                    <label>coverImage</label>
                                </div>
								<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="file" class="form-text" id="img"  required name="banner"/>
                                    <span class="bar"></span>
                                    <label>Add Banner</label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="file" class="form-text" id="images" name="images[]" onchange="preview_images();" multiple/>
                                    <span class="bar"></span>
                                    <label>Store Multiple Image</label>
                                </div>


                                <div class="row" id="image_preview"></div>
                                <div class="col-md-12">

                                    <input class="submit btn  btn-gradient btn-info" type="submit" value="Submit">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




