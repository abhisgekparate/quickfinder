		
		<link rel="stylesheet" href="<?php echo base_url();?>dist/css/selectize.default.css">
		<script src="<?php echo  base_url();?>js/jquery.min.js"></script>
		<script src="<?php echo  base_url();?>js/selectize.js"></script>
		<script src="<?php echo  base_url();?>js/index.js"></script>
		<style>
		.ap{
			padding:50px;
			border:1px solid #000;
		}
		.addbtn{
			padding: 5px 18px !important;
			margin: 0px !important;
		}
		.btnset
		{
			height: 160px !important;
		}
		.bar
		{
			color:red;
		}
		</style>
<div id="content">
            <div class="tabs-wrapper text-center">             
             <div class="panel box-shadow-none text-left content-header">
                  <div class="panel-body" style="padding-bottom:0px;">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Add Store</h3>
                        <p class="animated fadeInDown">
                        Plan <span class="fa-angle-right fa"></span> <?php
								if($this->session->userdata('plan')==1) 
									echo 'Entry Plan';
								elseif($this->session->userdata('plan')==2) 
									echo 'Silver Plan';
								elseif($this->session->userdata('plan')==3) 
									echo 'Gold Plan';
								elseif($this->session->userdata('plan')==4) 
									echo 'Platinum Plan';			
						?>
                        </p>
                    </div>
                   
                  </div>
              </div>
            <div class="col-md-12 tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="tabs-area-demo" aria-labelledby="tabs1">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <div class="col-md-12 tabs-area">
                      <div class="liner"></div>
                      <ul class="nav nav-tabs nav-tabs-v5" id="tabs-demo6">
                        <li class="active">
                         <a href="#tabs-demo6-area1" data-toggle="tab" title="welcome">
                          <span class="round-tabs one">
                            <i class="glyphicon glyphicon-home"></i>
                          </span> 
                        </a>
                      </li>

                      <li>
                        <a href="" data-toggle="tab" title="profile">
                         <span class="round-tabs two">
                           <i class="glyphicon glyphicon-user"></i>
                         </span> 
                       </a>
                     </li>

                     <!--<li>
                      <a href="#tabs-demo6-area3" data-toggle="tab" title="bootsnipp goodies">
                       <span class="round-tabs three">
                        <i class="glyphicon glyphicon-gift"></i>
                      </span> </a>
                    </li>

                   <li>
                      <a href="#tabs-demo6-area4" data-toggle="tab" title="blah blah">
                       <span class="round-tabs four">
                         <i class="glyphicon glyphicon-comment"></i>
                       </span> 
                     </a>
                   </li>

                   <li><a href="#tabs-demo6-area5" data-toggle="tab" title="completed">
                     <span class="round-tabs five">
                      <i class="glyphicon glyphicon-ok"></i>
                    </span> </a>
                  </li>-->
                </ul>
                <div class="tab-content tab-content-v5">
                  <div class="tab-pane fade in active" id="tabs-demo6-area1">

                    <div class="col-md-12">
					 <form class="cmxform" id="" method="post" action="<?php echo site_url('addnewstore')?>" enctype="multipart/form-data">
                      <div id="signupForm"></div>
					  <input type="hidden"  name="country" value="philippines"/>
					   <input type="hidden"  name="state" value="philippines"/>
					 <div class="col-md-6">
                      <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" id="validate_firstname" name="name" required >
                                    <span class="bar" id="name_err"><?php echo form_error('name', '<div class="error">', '</div>'); ?></span>
                                    <label>Store Name</label>
                      </div>

                       <div class="form-group form-animate-text" style="margin-top:40px !important;">

                                    <select class="form-text" name="categoryid">
                                        <option value="0" selected="selected">Select category</option>
                                        <?php
                                        print_r($categorylist);
                                        foreach ($categorylist as $row) {
                                            ?>
                                            <option value="<?php echo $row['category']['id']; ?>"><?php echo ucwords($row['category']['name']); ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
									<span class="bar" id="category_err"><?php echo form_error('categoryid', '<div class="error">', '</div>'); ?></span>
                                </div>
                      <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="email" class="form-text" id="validate_firstname" name="email" required>
                                    <span class="bar" id="email_err"><?php echo form_error('email', '<div class="error">', '</div>'); ?></span>
                                    <label>Store Email</label>
                                </div>
<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" minlength="10" maxlength="10" id="validate_firstname" name="mobile" required>
                                     <span class="bar" id="mobile_err"><?php echo form_error('mobile', '<div class="error">', '</div>'); ?></span>
                                    <label>Store Mobile</label>
                                </div>
                                
					  
                    </div>
                    <div class="col-md-6">
					
								<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="text" class="form-text" name="description" required/>
                                    <!-- <input type="text" class="form-text" id="validate_firstname" name="name" required> -->
                                   <span class="bar" id="description_err"><?php echo form_error('description', '<div class="error">', '</div>'); ?></span>
                                    <label>Store Descripion</label>
                                </div>
                     <!-- <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                   
									<select class="form-text country" name="country" >
                                        <option value="0">Select Country</option>
                                        <?php
                                        foreach ($country as $row) {
                                            ?>
                                            <option id="<?php echo $row['id']; ?>" value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>e
                                </div>
						<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text states" name="state" >
                                        <option value="0">Select State</option>
                                      <!--   <?php
                                        foreach ($state as $row) {
                                            ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?>                                     </select>
                                </div>-->

                      <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <select class="form-text cities" name="city" id="city">
                                        <option  selected="selected">Select City</option>
                                        <option value="cebu">Cebu</option>
                                        <option value="manila">Manila</option>
                                        <!-- <?php
                                        foreach ($city as $row) {
                                            ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo ucwords($row['name']); ?></option>

                                            <?php
                                        }
                                        ?> -->
                                    </select>
                                     <span class="bar" id="city_err"><?php echo form_error('city', '<div class="error">', '</div>'); ?></span>
                                </div>
								<div class="form-group form-animate-text" style="margin-top:40px !important;">
                                   
                                        <select class="form-text area" name="area" id="area">
                                        <option value="0" selected="selected">Select Area</option>
                                        <option value="apas">Apas</option>
                                        <option value="banilad">Banilad</option>
                                        <option value="basak pardo">Basak Pardo</option>
                                        <option value="capitol site">Capitol Site</option>
                                       <option value="guadalupe">Guadalupe</option>
                                       <option value="kamputhaw">Kamputhaw</option>
                                       <option value="kasambagan">Kasambagan</option>
                                       <option value="labangon">Labangon</option>
                                       <option value="lahug">Lahug</option>
                                       <option value="mabolo">Mabolo</option>
                                        <option value="talamban">Talamban</option>
                                        <option value="mandaue">Mandaue</option>
                                        
                                    </select>
                                    <span class="bar" id="area1_err"><?php echo form_error('area1', '<div class="error">', '</div>'); ?></span>
                                </div>
						
                      
					 
                    </div>
						<div class="col-md-12">
                              
                              <a href="" data-toggle="tab" id="next1"><input class="submit btn btn-danger" type="button" value="NEXT"></a>
                        </div>
						
                  </div>
                        
                           
				 </div>
				 
					
                  <div class="tab-pane fade" id="tabs-demo6-area2" >
				 <div class="col-md-12">
                   <div class="col-md-6">
				   
				   <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="file" class="form-text" id="img"   name="img"/>
                          <span class="bar"></span>
                          <label>coverImage</label>
                    </div>
					</div>
					<div class="col-md-6">	
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input type="file" class="form-text" id="images" name="images[]" onchange="preview_images();" multiple/>
                                    <span class="bar"></span>
                                    <label>Store Multiple Image</label>
                                </div>
								</div>
                  </div>
				   <div class="col-md-12">
                              
                             <input class="btn box-shadow-none text-white bg-indigo" type="button" id="next2" value="Prev"><input class="submit btn btn-danger" type="submit" value="Submit">
                        </div>
					</div>	
                  <div class="tab-pane fade" id="tabs-demo6-area3" >
				  
                    <h3 class="head text-center">store<sup>™</sup> <span style="color:#f48250;">♥</span></h3>
                    <p class="narrow text-center">
                      Lorem ipsum dolor sit amet, his ea mollis fabellas principes. Quo mazim facilis tincidunt ut, utinam saperet facilisi an vim.
                    </p>

                    <p class="text-center">
                      <a href="" class="btn btn-success btn-round green"> start using Mimin <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></a>
                    </p>
                  </div>
				 
				  
				  </form>
                  <div class="tab-pane fade" id="tabs-demo6-area4">
                   <div class="col-md-12">
				     <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="text" class="form-text dateAnimate" name="date" required>
                        <span class="bar"></span>
                        <label>Next Date of Follow up</label>
                      </div>
						 <div class="row">
						 <?php 
						
						 
						 foreach($cal as $row)
						 {
								if($row['count']==0)
								{
									$hight='0px';
								}
								elseif($row['count']==1)
								{
									$hight='47px';
								}
								elseif($row['count']==2)
								{
									$hight='95px';
								}
								elseif($row['count']==3)
								{
									$hight='140px';
								}
						 ?>
              <div class="col-sm-6 col-md-3">
			  <div class="btnset">
                <div class="panel bg-primary box-shadow-none" style="height:<?php echo $hight;?>">
                  <div class="panel-body ap">
                    <center><h4 class="text-white" style="color:#000 !important;"><?php echo $row['name']; ?></h4></center>
                  </div>
                </div>
				</div>
				<input type="button" class="btn btn-success addbtn" value="Add" name=""/>
				<span class="label label-primary">Available :</span><span class="label label-danger"> <?php echo 3-$row['count'];?></span>
              </div>
						 <?php }?>
             
				   </div>
                  </div>
				  </div>
                  <div class="tab-pane fade" id="tabs-demo6-area5">
                   
					<label for="select-state">Keywords </label>
					<select id="select-state" multiple name="tags[]" class="demo-default" style="width:50%">
						
						<option value="VT">Vermont</option>
						<option value="VA">Virginia</option>
						<option value="WA">Washington</option>
						<option value="WV">West Virginia</option>
						<option value="WI">Wisconsin</option>
						<option value="WY" selected>Wyoming</option>
					</select>
					<div class="col-md-12">
                              
                              <a href="#tabs-demo6-area3" data-toggle="tab" id="next2"><input class="submit btn btn-danger" type="button" value="NEXT"></a>
                        </div>
				<script>
				var eventHandler = function(name) {
				
					return function() {
						console.log(name, arguments);
						$('#log').append('<div><span class="name">' + name + '</span></div>');
					};
				};
				var $select = $('#select-state').selectize({
					create          : true,
					onChange        : eventHandler('onChange'),
					onItemAdd       : eventHandler('onItemAdd'),
					onItemRemove    : eventHandler('onItemRemove'),
					onOptionAdd     : eventHandler('onOptionAdd'),
					onOptionRemove  : eventHandler('onOptionRemove'),
					onDropdownOpen  : eventHandler('onDropdownOpen'),
					onDropdownClose : eventHandler('onDropdownClose'),
					onFocus         : eventHandler('onFocus'),
					onBlur          : eventHandler('onBlur'),
					onInitialize    : eventHandler('onInitialize'),
				});
				</script>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>      
              </div>
             
                
                
                </div>
                
              </div>
            </div>
          </div>