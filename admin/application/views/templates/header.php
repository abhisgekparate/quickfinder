
<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Quickfinder</title>
 <style type="text/css">
.error
{color:red;}
.upload {
  width: 100%;
  height: 30px;
}
.previewBox {
  text-align: center;
  position: relative;
   padding: 5px;
  width: 50px;
  height: 50px;
  margin-right: 10px;
  margin-bottom: 20px;
  float: left;
}
.previewBox img {
  height: 50px;
  width: 50px;
  padding: 5px;
  border: 1px solid rgb(232, 222, 189);
}
.delete {
  color: red;
  font-weight: bold;
  position: absolute;
  top: 0;
  cursor: pointer;
  width: 20px;
  height:  20px;
  border-radius: 50%;
  background: red;
}
</style>
    <!-- start: Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/bootstrap.min.css">

      <!-- plugins -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/font-awesome.min.css"/>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/nouislider.min.css"/>
	 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/select2.min.css"/>
	 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/ionrangeslider/ion.rangeSlider.css"/>
	 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/ionrangeslider/ion.rangeSlider.skinFlat.css"/>
	 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/bootstrap-material-datetimepicker.css"/>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/simple-line-icons.css"/>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/animate.min.css"/>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/fullcalendar.min.css"/>
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/datatables.bootstrap.min.css"/>
	 <link href="<?php echo base_url();?>asset/css/style.css" rel="stylesheet">
	   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/plugins/dropzone.css"/>
	  
 
	 <!-- end: Css -->
	<link rel="shortcut icon" href="<?php echo base_url();?>asset/img/logomi.png">
  <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
 <!--  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tag-it/2.0/css/jquery.tagit.min.css.map"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/jquery.tagit.css""/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/tagit.ui-zendesk.css""/>
  <!-- tag in -->
  <link rel="stylesheet" href="<?php echo base_url();?>/asset/css/bootstrap-tagsinput.css">
  </head>

 <body id="mimin" class="dashboard">
      <!-- start: Header -->
        <nav class="navbar navbar-default header navbar-fixed-top">
          <div class="col-md-12 nav-wrapper">
            <div class="navbar-header" style="width:100%;">
              <div class="opener-left-menu is-open">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
              </div>
                <a href="<?php echo site_url('home');?>" class="navbar-brand"> 
                 <b>Quickfinder</b>
                </a>

              
              <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span><?php echo $this->session->userdata('email');?></span></li>
                  <li class="dropdown avatar-dropdown">
                   <img src="<?php echo base_url();?>asset/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                   <ul class="dropdown-menu user-dropdown">
                     <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                     <li><a href="#"><span class="fa fa-calendar"></span> My Calendar</a></li>
                     <li role="separator" class="divider"></li>
                     <li class="more">
                      <ul>
                        <li><a href=""><span class="fa fa-cogs"></span></a></li>
                        <li><a href=""><span class="fa fa-lock"></span></a></li>
                        <li><a href="<?php echo site_url('logout');?>"><span class="fa fa-power-off "></span></a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <!--<li ><a href="#" class="opener-right-menu"><span class="fa fa-coffee"></span></a></li>-->
              </ul>
            </div>
          </div>
        </nav>
      <!-- end: Header -->
