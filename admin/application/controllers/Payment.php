<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Payment_model');
	}
	public function index()
	{
		$data['payment']=$this->Payment_model->getPaymentData();
		$data['page']='payment/paymentlist';
		$this->load->view('templates/content',$data);
	}
	public function details($id)
	{
		$data['page']='payment/paymentdetails';
		$this->load->view('templates/content',$data);
	}
}	
?>