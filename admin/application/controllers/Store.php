<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Store_model');
        $this->load->model('Category_model');
    }

    public function index() {
        $data['storelist'] = $this->Store_model->getStores();
		
		if($num_store=$this->Store_model->getNumStore())
		{
			$this->session->set_userdata('num_store',$num_store);
		}
        $data['page'] = 'store/storelist';
        $this->load->view('templates/content', $data);
    }

    public function newStore() {
		
        $data['categorylist'] = $this->Category_model->getCategory();
        $data['country'] = $this->Store_model->getCountry();
         //$data['state'] = $this->Store_model->getState();
        // $data['city'] = $this->Store_model->getCities();
		//$data['page'] = 'store/addstore';
		$data['cal']=$this->Store_model->getCalenderData();
		$data['page'] = 'store/demoaddstore';
        $this->load->view('templates/content', $data);
    }
	 public function newStore1() {
        $data['categorylist'] = $this->Category_model->getCategory();
        $data['country'] = $this->Store_model->getCountry();
         //$data['state'] = $this->Store_model->getState();
        // $data['city'] = $this->Store_model->getCities();
		//$data['page'] = 'store/addstorewithbanner';
		$data['page'] = 'store/addstorewithbanner1';
        $this->load->view('templates/content', $data);
    }
    public function getState(){
        
        $country_id = $_POST['country_id'];
        $states = $this->Store_model->getStateData($country_id);
        if(count($states) > 0){

            $state_box='';
            $state_box.='<option>Select State</option>';

            foreach ($states as $state) {
                $state_box.='<option id="'.$state['id'].'" value="'.$state['name'].'">'.$state['name'].'</option>';
            }
            echo json_encode($state_box);
        }

    }

public function getCity(){

        $state_id = $_POST['state_id'];
        // print_r($_POST);
        // die;
        $cities = $this->Store_model->getCities($state_id);
        if(count($cities) > 0){

            $city_box='';
            $city_box.='<option>Select City</option>';

            foreach ($cities as $city) {
                $city_box.='<option value="'.$city['id'].'">'.$city['name'].'</option>';
            }
            echo json_encode($city_box);
        }

    }

    public function addNewStore() {

       
		$filesCount = count($_FILES['images']['name']);
		//multiple image upload code
        $uploadData= array();
        for ($i = 0; $i < $filesCount; $i++) {

            $_FILES['image']['name'] = $_FILES['images']['name'][$i];
            $_FILES['image']['type'] = $_FILES['images']['type'][$i];
            $_FILES['image']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['image']['error'] = $_FILES['images']['error'][$i];
            $_FILES['image']['size'] = $_FILES['images']['size'][$i];

            $uploadPath = './storeimages/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $fileData = $this->upload->data();
				$uploadData[$i]['file_name'] = 'storeimages/' . $fileData['file_name'];
            }
        }
		//create store with single image upload code
        // Upload the files then pass data to your model
        $config['upload_path'] = './storeimages/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('img')) {
            // If the upload fails
            echo $this->upload->display_errors('<p>', '</p>');
        } else {
            // Pass the full path and post data to the set_newstudent model
            $filedata = $this->upload->data();
            $filename = 'storeimages/' . $filedata['file_name'];
            if ($this->Store_model->addNewStore($filename, $uploadData)) {
                redirect('Store');
            }
        }
    }
	public function addNewStoreP() {

       
		$filesCount = count($_FILES['images']['name']);
		//multiple image upload code
        $uploadData= array();
        for ($i = 0; $i < $filesCount; $i++) {

            $_FILES['image']['name'] = $_FILES['images']['name'][$i];
            $_FILES['image']['type'] = $_FILES['images']['type'][$i];
            $_FILES['image']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
            $_FILES['image']['error'] = $_FILES['images']['error'][$i];
            $_FILES['image']['size'] = $_FILES['images']['size'][$i];

            $uploadPath = './storeimages/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $fileData = $this->upload->data();
				$uploadData[$i]['file_name'] = 'storeimages/' . $fileData['file_name'];
            }
        }
		//upload banner
		
        if ($this->upload->do_upload('banner')) {
		$banner = $this->upload->data();
		
		$bannerfile = 'storeimages/' . $banner['file_name'];
		
		//create store with single image upload code
        // Upload the files then pass data to your model
        $config['upload_path'] = './storeimages/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);

			if (!$this->upload->do_upload('img')) {
				// If the upload fails
				echo $this->upload->display_errors('<p>', '</p>');
			} else {
				// Pass the full path and post data to the set_newstudent model
				$filedata = $this->upload->data();
				
				$filename = 'storeimages/' . $filedata['file_name'];
				if ($this->Store_model->addNewStore1($filename,$bannerfile, $uploadData)) {
					redirect('Store');
				}
			}
		}
    }
	public function pricing()
	{
		 $data['page'] = 'pricing/pricing';
         $this->load->view('templates/content', $data);
	}
	public function pricingp($id)
	{
		 $this->session->set_userdata('store_id',$id);
		 $data['page'] = 'pricing/pricingpayment';
         $this->load->view('templates/content', $data);
	}
	public function plan($id)
	{
		$this->session->set_userdata('plan',$id);
		if($id==4)
		{
			$this->newStore1();
		}
		else{
		$this->newStore();
		}
	}
	public function planP($id)
	{
		if($id==4)
		{
			$this->session->set_userdata('plan',4);
			$data['page'] = 'pricing/addBanner';
            $this->load->view('templates/content', $data);
		}
		else
		{
			$store_id= $this->session->userdata('store_id');
			if($this->Store_model->paymentPay($id,$store_id))
			{
				redirect('Store');
			}
		}
	}
	public function planB($id)
	{
		$uploadPath = './storeimages/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
		$store_id= $this->session->userdata('store_id');
		echo $store_id;
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('banner'))
		{
			
			$banner = $this->upload->data();
			
			$bannerfile = 'storeimages/' . $banner['file_name'];
			$store_id= $this->session->userdata('store_id');
			
			if($this->Store_model->paymentPay($id,$store_id))
			{
				if($this->Store_model->platinumPlanB($id,$store_id,$bannerfile))
				{
					$this->session->unset_userdata('plan');
					redirect('Store');
				}
			}
		}
	}

}

?>