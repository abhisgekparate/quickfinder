<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Category_model');
		$this->load->model('store_model');
	}	
	public function index()
	{
		$data['categorylist']=$this->Category_model->getCategory();	
		$data['page']='category/categorylist';
		$this->load->view('templates/content',$data);	
	}
	public function addCategory()
	{
		$data['page']='category/addcategory';
		$this->load->view('templates/content',$data);	
	}
	public function viewSubCategory($id)
	{
		$data['subcategory']=$this->Category_model->getSubCategory($id);
		$data['page']='category/addsubcategory';
		$this->load->view('templates/content',$data);	
	}
	public function addSubCategory()
	{
		$this->Category_model->addSubCategory();
		{
			redirect('Category');
		}
	}
	public function addNewCategory()
	{
		if($this->Category_model->addNewCategory())
		{
			redirect('Category');
		}
	}
	public function serch()
	{
		
		$city=$_POST['id'];
		$searchBox=$_POST['srchbx'];
		$area=$_POST['area'];
		
		$newstore = $this->store_model->getStoresInfoBySearchBox($city,$searchBox,$area);
		if(is_array($newstore))
					   {
						   $i=0;
						   foreach ($newstore as $row)
						   {
							 
						   ?>
						    <!--- card start------->
							<figure class="figure pull-left fadeInLeftShort animated ">
					<div class="col-md-12 n-margin-2">
						   <div class="col-md-4 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url('about/'.$row['id'])?>"> <img src="<?php echo base_url();?><?php echo $row['firstimage']?>" class="box-v2-cover img-responsive n-img"></a>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-12 ">
                            
                             <h3 class="vender-name"><?php echo $row['name'];?></h3>
                             <select class="example-css">
							  <option value="1">1</option>
							  <option value="2">2</option>
							  <option value="3">3</option>
							  <option value="4">4</option>
							  <option value="5">5</option>
							</select>
                             <p class="contact"><span class="fa-phone fa contact-icon"></span><?php echo $row['mobile'];?></p>
                             
                             <p class="address"><span class="fa-diamond fa contact-icon"></span><?php echo substr($row['description'],0,25);?>
							</p>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 price">
                        </div>
						 </div>
						</figure>
						<script src="<?php echo base_url();?>asset1/js/plugins/Minimal-jQuery-Rating-Widget-Plugin-Bar-Rating/examples.js"></script>
                        <?php
						}
					   }
					   else{
						   echo $newstore;
					   }
	}
	//display store by category id
	public function pages($id)
	{
		$newstore = $this->store_model->getStoresInfoByCategory($id);
					   if(is_array($newstore))
					   {
						   $i=0;
						   foreach ($newstore as $row)
						   {
							 
						   ?>
						    <!--- card start------->
							<figure class="figure pull-left fadeInLeftShort animated ">
              <div class="col-md-12 n-margin-2">
						   <div class="col-md-4 col-sm-6 col-xs-12">
                            <a href="<?php echo site_url('about/'.$row['id'])?>"> <img src="<?php echo base_url();?><?php echo $row['firstimage']?>" class="box-v2-cover img-responsive n-img"></a>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-12 ">
                            
                             <h3 class="vender-name"><?php echo $row['name'];?></h3>
                             <select class="example-css">
							  <option value="1">1</option>
							  <option value="2">2</option>
							  <option value="3">3</option>
							  <option value="4">4</option>
							  <option value="5">5</option>
							</select>
                            
                             <p class="contact"><span class="fa-phone fa contact-icon"></span><?php echo  $row['mobile'];?></p>
                             
                             <p class="address"><span class="fa-diamond fa contact-icon"></span><?php echo  substr($row['description'],0,50);?>
							</p>
                        </div>
                      
                        <div class="col-md-2 col-sm-6 col-xs-12 price">

                        </div>
						 </div>
              <!--- card end-------> 
  </figure>
           <script src="<?php echo base_url();?>asset1/js/plugins/Minimal-jQuery-Rating-Widget-Plugin-Bar-Rating/examples.js"></script>			  
                        <?php
						}
					   }
					   else{
						   echo $newstore;
					   }

		 
	}
	
}
?>