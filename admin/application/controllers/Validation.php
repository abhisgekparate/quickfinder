<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validation extends CI_Controller {
	public function __construct()
	{
		parent::__construct();		
	}	
	public function index()
	{
	}
	public function addStoreNext1()
	{
		$this->form_validation->set_rules('name', '', 'required');
        $this->form_validation->set_rules('categoryid', '', 'required');
        $this->form_validation->set_rules('email', '', 'required|valid_email');
        $this->form_validation->set_rules('mobile', '', 'required');
        $this->form_validation->set_rules('city', '', 'required');
        $this->form_validation->set_rules('area1', 'area', 'required');
        $this->form_validation->set_rules('description', '', 'required');
        if ($this->form_validation->run() == FALSE){
			
			$array='';
			foreach($this->form_validation->error_array() as $key => $val)
			{
				$array[$key]=$val;
			}
			//print_r($array);
			echo json_encode($array);
		  // $errors = validation_errors();
            //echo json_encode(['error'=>$errors]);
        }else{
           echo json_encode(['sucess'=>'1']);
        }
	}
	
}