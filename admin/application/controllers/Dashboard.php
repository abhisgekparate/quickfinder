<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			$this->load->view('signIn');
		}		
		$this->load->model('Dashboard_model');
	}	
	public function index()
	{
		$data['visits']=$this->Dashboard_model->getTotalVisits();
		$data['stores']=$this->Dashboard_model->getTotalStores();
		
		$data['page'] = 'home';
		$this->load->view('templates/content',$data);
	}
}