<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('store_model');
	}	
	public function index()
	{
		$data['store'] = $this->store_model->getStoreInfo();
		$data['menu'] = $this->store_model->getMenu();
		$data['city'] = $this->store_model->getCities();
		$this->load->view('client/welcome',$data);
	}
	public function about($id)
	{
		$data['userView']=$this->store_model->updateUserView($id,1);
		$data['store'] = $this->store_model->getStoreData($id);
		$data['storeimage'] = $this->store_model->getStoreImages($id);
		$this->load->view('client/storepage',$data);
	}
}
