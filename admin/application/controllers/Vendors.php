<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Vendors_model');
	}
	public function index()
	{
		$data['venders']=$this->Vendors_model->getVendorsList();
		
		$data['page'] = 'vendors/vendorlist';
		$this->load->view('templates/content',$data);
	}
}
?>