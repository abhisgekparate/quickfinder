<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Register_model');
		$this->load->model('store_model');
	}	
	public function index()
	{
		$data['page'] = 'home';
		$this->load->view('templates/content',$data);
	}
	public function login()
	{
		
		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			$this->load->view('signIn');
		}
		else
		{
			redirect('dashboard');
		}
		
	}
	public function userLogin()
	{
		
		$this->form_validation->set_rules('username', '', 'required');
		$this->form_validation->set_rules('password', '', 'required');
		if($this->form_validation->run() == FALSE)
          {
				$this->load->view('signIn');
          }
         else
            {
               if($data=$this->Register_model->login())
				{
					$this->session->set_userdata(array('userid'=>$data[0]['id'],'usertype'=>$data[0]['usertype'],'email'=>$data[0]['email'],'is_logged_in'=>TRUE,'is_logged'=>1));
					if($num_store=$this->store_model->getNumStore())
					{
						$this->session->set_userdata('num_store',$num_store);
					}
					redirect('dashboard');
				}
				else
				{
					$this->session->set_userdata('errmsg','UserName And Password Missmatch!...');
					$this->load->view('signIn');
				}
            }
	}
	public function viewRegister()
	{
		if($this->session->userdata('is_logged')!=1 && $this->session->userdata('is_logged_in')!=TRUE)
		{			
			$this->load->view('signUp');
			
		}
		else
		{
			redirect('dashboard');
		}
	}
	public function Register()
	{
		$this->form_validation->set_rules('mobile', 'Plz Enter The mobile', 'required');
		$this->form_validation->set_rules('password', '', 'required|min_length[6]|max_length[15]');
		$this->form_validation->set_rules('email', 'Email', 'required');
		if($this->form_validation->run() == FALSE)
          {
                $data['page'] = 'signUp';
				$this->load->view('signUp');
          }
         else
            {
				if($this->Register_model->checkEmail())
				{
				   if($this->Register_model->register())
					{
						$this->session->set_userdata('msg',"Register Successfully!..");
						$this->load->view('signIn');
					}
					else
					{
						$this->load->view('signUp');
					}
				}
				else
				{
					$this->session->set_userdata('errmsg',"Email allready Exists!..");
					$this->load->view('signUp');
				}		
            }	
	}
	public function userRegister()
	{
		
		if($this->Register_model->register())
		{
			
			$this->load->view('signIn');
		}
		else
		{
			$data['page'] = 'signUp';
			$this->load->view('signUp');
		}
		
	}
	public function logout()
	{	
		
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
		$this->session->sess_destroy();
		$this->session->set_userdata('is_logged_in',FALSE);
		$this->session->set_userdata('is_logged',2);
		redirect('signin');
	}
}
