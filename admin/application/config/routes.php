<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'register/login';
$route['home'] = 'login';
$route['about/(:num)'] = 'login/about/$1';
//register
$route['signup'] = 'register/viewRegister';
$route['signin'] = 'register/login';
$route['register'] = 'register/Register';
//login
$route['login'] = 'register/userLogin';
//logOut
$route['logout'] = 'register/logout';
//categories
$route['categories'] = 'Category';
$route['category/(:num)'] = 'Category/pages/$1';
$route['categorywise/serch'] = 'Category/serch';
//addCategory
$route['addCategory'] = 'Category/addCategory';
$route['addNewCategory'] = 'Category/addNewCategory';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
//addSubCategory
$route['addSubCategory/(:num)'] = 'Category/viewSubCategory/$1';
$route['addSubCategory/addnew'] = 'Category/addSubCategory';
//manage store
$route['storelist'] = 'store';
$route['addstore'] = 'Store/newStore';
$route['addnewstore'] = 'Store/addNewStore';
$route['addnewstoreP'] = 'Store/addNewStoreP';
$route['Store/getState'] = 'Store/getState';
$route['Store/getCity'] = 'Store/getCity';
//pricing
$route['pricing'] = 'Store/pricing';
$route['pricingp/(:num)'] = 'Store/pricingp/$1';
$route['SilverPlan'] = 'Store/plan/2';
$route['EntryPlan'] = 'Store/plan/1';
$route['GoldPlan'] = 'Store/plan/3';
$route['PlatinumPlan'] = 'Store/plan/4';
$route['SilverPlanP'] = 'Store/planP/2';
$route['EntryPlanP'] = 'Store/planP/1';
$route['GoldPlanP'] = 'Store/planP/3';
$route['PlatinumPlanP'] = 'Store/planP/4';
$route['PlatinumPlanB'] = 'Store/planB/4';
//validation
$route['next1validate'] = 'Validation/addStoreNext1';
//payment
$route['paymentdetails/(:num)'] = 'payment/details/$1';
//venders
