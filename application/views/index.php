<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quick Finder</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <!-- Custom Theme files -->
    <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>

    <!--- materialize css --->
    <link href="<?php echo base_url();?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="<?php echo base_url();?>css/menu.css" rel="stylesheet" type="text/css" media="all"/> <!-- menu style -->

    <link href="<?php echo base_url();?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/> <!-- banner slider -->
    <link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url();?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all"> <!-- carousel slider -->


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo base_url();?>css/imagehovereffects.css" rel="stylesheet">

    <link href="<?php echo base_url();?>css/circularwaves.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="<?php echo base_url();?>js/jquery-3.2.1.min.js"></script>
    <!-- //js -->

    <!---- materialize js ---->
    <script src="<?php echo base_url();?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url();?>js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

            //$('.header-two').scrollToFixed();
            // previous summary up the page.
            /*
                    var summaries = $('.summary');
                    summaries.each(function(i) {
                        var summary = $(summaries[i]);
                        var next = summaries[i + 1];

                        summary.scrollToFixed({
                            marginTop: $('.header-two').outerHeight(true) + 10,
                            zIndex: 999
                        });
                    });*/
        });
    </script>
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({easingType: 'easeOutQuart'});

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->


</head>
<body>
<div class="agileits-modal modal fade" id="myModal88" tabindex="-1" role="dialog" aria-labelledby="myModal88"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-map-marker" aria-hidden="true"></i> Location
                </h4>
            </div>
            <div class="modal-body modal-body-sub">
                <h5>Select your delivery location </h5>
                <select class="form-control bfh-states" data-country="US" data-state="CA">
                    <option value="">Select Your location</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AS">American Samoa</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District of Columbia</option>
                    <option value="FM">Federated States Of Micronesia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="GU">Guam</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PW">Palau</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VI">Virgin Islands</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>
                <input type="text" name="Name" placeholder="Enter your area / Landmark / Pincode" required="">
                <button type="button" class="close2" data-dismiss="modal" aria-hidden="true">Skip & Explore</button>
            </div>
        </div>
    </div>
</div>


<datalist id="anything">
    <option value="Hotels">Hotels</option>
    <option value="Restarants">Restarants</option>
    <option value="Autocare">AutoCare</option>
</datalist>

<datalist id="cities">
    <option value="Cebu">Cebu</option>
    <option value="Manila">Manila</option>
</datalist>

<datalist id="areas">
    <option value="All">All</option>
    <option value="Soriano Avenue">Soriano Avenue</option>
    <option value="J. de Veyra Street">J. de Veyra Street</option>
</datalist>



<div class="n-ele-center n-white">
    <!-- header -->

    <div class="header">
        <div class="header-two"><!-- header-two -->
            <div class="container">
                <div class="header-logo">
                    <h1><a href="<?php echo site_url();?>"><img class="logo" src="<?php echo base_url();?>images/logo/1.png" alt="quickfinder logo"/></a></h1>
                </div>
                <div class="header-search">
					<form action="<?php echo site_url('Search')?>" method="post"/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <input type='search'
                                   placeholder='Search anything in Philippines'
                                   class='productsearch'
                                   data-min-length='0'
                                   list='anything'
                                   name='search' />
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   class='citysearch'
                                   data-min-length='0'
                                   list='cities'
                                   placeholder='Select city'
                                   name='flexdatalist-city'
                                   value="Cebu">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   placeholder='Select Area'
                                   class='areasearch'
                                   data-min-length='0'
                                   list='areas'
                                   name='area'
                                   value="All">
                        </div>
                    </div>
				
                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer searchbtn">
                            <button type="submit" class="btn btn-default" aria-label="Left Align">
                                <i class="fa fa-search" aria-hidden="true"> </i>
                            </button>
                        </div>
					</div>
				</form>

                </div>
				<div class="col-md-2 social-icons">
				<ul class="apps" style="margin-top:3em !important;">
				<?php if($this->session->userdata('userid'))
			  {
				   ?>
                    <li><a href="<?php echo site_url('dashboard');?>" class="">My Profile</a></li>
                     <li><a href="#" >/</a></li>
					<li><a href="<?php echo site_url('logout');?>" class="">logout</a></li>
                    
					 <?php }else{?>
					 <li><a href="<?php echo site_url('admin/signin');?>" >Login</a></li>
					 <li><a href="#" >/</a></li>
                    <li><a href="<?php echo site_url('admin/signup');?>" >Register</a></li>
                   
					 
					 <?php				
			  } ?>
                </ul>
				</div>
				 <li class="login" style="display:none">
             
			  
				 
                
                <div class="clearfix"></div>

            </div>
        </div><!-- //header-two -->
    </div>


    <!-- //header -->
    <!-- banner -->
    <div class="banner pad-tp">
        <div class="carousel carousel-slider center ">
            <div class="carousel-fixed-item center middle-indicator">

                <div class="slider-btn-container">
                    <div class="left">
                        <a href="Previo"
                           class="movePrevCarousel middle-indicator-text waves-effect waves-light content-indicator"><i
                                class="material-icons left  middle-indicator-text">chevron_left</i></a>
                    </div>

                    <div class="right">
                        <a href="Siguiente"
                           class=" moveNextCarousel middle-indicator-text waves-effect waves-light content-indicator"><i
                                class="material-icons right middle-indicator-text">chevron_right</i></a>
                    </div>
                </div>

            </div>
            <a class="carousel-item" href="#one!">
                <img src="<?php echo base_url();?>images/slider/1.png" alt="banner1">
            </a>

            <a class="carousel-item" href="#two!">
                <img src="<?php echo base_url();?>images/slider/2.png" alt="banner2">
            </a>

            <a class="carousel-item" href="#three!">
                <img src="<?php echo base_url();?>images/slider/3.png" alt="banner3">
            </a>
        </div>

        <script>
            $(document).ready(function () {
                var timer;

                $('.carousel.carousel-slider').carousel({
                    fullWidth: true,
                    indicators: false
                });

                // move next carousel
                $('.moveNextCarousel').click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('.carousel').carousel('next');

                    /***reset timer*/
                    clearTimeout(timer);
                    autoplay();
                });

                // move prev carousel
                $('.movePrevCarousel').click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('.carousel').carousel('prev');

                    /***reset timer*/
                    clearTimeout(timer);
                    autoplay();
                });

                /***Default auto play***/
                autoplay();

                function  autoplay() {
                    timer=setInterval(function() {
                        $('.carousel').carousel('next');
                    }, 8000); // every 2 seconds

                }

            });

        </script>
    </div>
    <!-- //banner -->
    <!-- deals -->
    <div class="deals">
        <div> <!--class="container"> -->
            <div>

            </div>
            <h3 class="w3ls-title">Categories </h3>

            <div class="deals-row">
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree2">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/1.png" alt="Professionals"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Professionals</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/jobs1.png" alt="Jobs"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Jobs</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree4">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/loans.png" alt="loans"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Loan</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/travel3.png" alt="travel"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Travel</h4>
                    </a>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/doctors2.png" alt="Doctors"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Doctors</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/fitness2.png" alt="Health"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Fitness</h4>
                    </a>
                </div>


                <div class="clearfix"></div>
            </div>

            <div class="deals-row">
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree2">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/saloon.png" alt="Saloon"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Saloon</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/spa1.png" alt="SPA"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">SPA</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree4">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/freelancer2.png" alt="Freelancers"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Freelancers</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/insurance.png" alt="Insurance"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Insurance</h4>
                    </a>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/shoponline3.png" alt="Shop Online"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Shop Online</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/onhire.png" alt="On Hire"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">On Hire</h4>
                    </a>
                </div>


                <div class="clearfix"></div>
            </div>

            <div class="deals-row">
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree2">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/homeservices2.png" alt="Home Services"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Home Services</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/laywers.png" alt="Laywers"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Laywers</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree4">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/wedding.png" alt="Wedding Requisites"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Wedding Requisites</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/entertainment1.png" alt="Entertainment"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Entertainment</h4>
                    </a>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/edu3.png" alt="Education"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Education</h4>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4  focus-grid pd-0">
                    <a href="subcategories.html" class="wthree-btn wthree3">
                        <div class="focus-image hover15 hover03 column">
                            <figure><img class="icon-img" src="<?php echo base_url();?>images/icons/autocare1.png" alt="Auto care"/>
                            </figure>
                        </div>
                        <h4 class="clrchg">Autocare</h4>
                    </a>
                </div>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
    <!-- //deals -->

    <!-- footer-top -->
    <div class="w3agile-ftr-top">
        <div class="">
            <div class="ftr-toprow">
                <div class="col-md-4 ftr-top-grids ">
                    <div class="ftr-top-left circle">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                    <div class="ftr-top-right">
                        <h4>CUSTOMER CARE</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 ftr-top-grids">
                    <div class="ftr-top-left circle">
                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                    </div>
                    <div class="ftr-top-right">
                        <h4>GOOD QUALITY</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 ftr-top-grids">
                    <div class="ftr-top-left circle">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                    <div class="ftr-top-right">
                        <h4>CUSTOMER CARE</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //footer-top -->
    <!-- subscribe -->
    <div class="subscribe">
        <div class="">
            <div class="col-md-6 social-icons w3-agile-icons">
                <h4>Keep in touch</h4>
                <ul>
                    <li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
                    <li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
                    <li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
                    <li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
                    <li><a href="#" class="fa fa-rss icon rss"> </a></li>
                </ul>
                <ul class="apps">
                    <li><h4>Download Our app : </h4></li>
                    <li><a href="#" class="fa fa-apple"></a></li>
                    <li><a href="#" class="fa fa-windows"></a></li>
                    <li><a href="#" class="fa fa-android"></a></li>
                </ul>
            </div>
            <div class="col-md-6 subscribe-right">
                <h4>Sign up for email</h4>
                <form action="#" method="post">
                    <input type="text" name="email" placeholder="Enter your Email..." required="">
                    <input type="submit" value="Subscribe">
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //subscribe -->
    <div class="copy-right">
        <div class="">
            <p>© 2018 All rights reserved <a href="http://"> Quickfinder</a></p>
        </div>
    </div>

</div>


<!-- menu js aim -->
<script src="js/jquery.menu-aim.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<!-- //menu js aim -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<!---auto suggest---->
<link href="<?php echo base_url();?>plugins/jquery-flexdatalist/jquery.flexdatalist.min.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url();?>plugins/jquery-flexdatalist/jquery.flexdatalist.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/countrysearch.js" type="text/javascript"></script>

<script>
    /*** detect click event source ***/
    //$( "body" ).click(function( event ) {
    // $( "#log" ).html( "clicked: " + event.target.nodeName );
    //	alert("clicked: " + event.target.nodeName );
    //});


    $(document).on("click", "div", function (e) {
        // doSomething();
        //alert();
        //e.preventDefault();
    });
</script>

<!--auto adjust height of all cols----->
<script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-match-height/jquery.matchHeight.js"></script>
<script>
    $(function() {
        $('.wthree-btn').matchHeight();
    });
</script>



</body>
</html>