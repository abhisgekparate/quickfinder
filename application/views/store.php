<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quick Finder</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <!-- Custom Theme files -->
    <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>

    <!--- materialize css --->
    <link href="<?php echo base_url();?>materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="<?php echo base_url();?>css/menu.css" rel="stylesheet" type="text/css" media="all"/> <!-- menu style -->

    <link href="<?php echo base_url();?>css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/> <!-- banner slider -->
    <link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo base_url();?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="all"> <!-- carousel slider -->


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo base_url();?>css/subcategories.css" rel="stylesheet">


    <link href="<?php echo base_url();?>css/n-card.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="<?php echo base_url();?>js/jquery-3.2.1.min.js"></script>
    <!-- //js -->

    <!---- materialize js ---->
    <script src="<?php echo base_url();?>materialize/dist/js/materialize.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url();?>js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

            //$('.header-two').scrollToFixed();
            // previous summary up the page.
            /*
                    var summaries = $('.summary');
                    summaries.each(function(i) {
                        var summary = $(summaries[i]);
                        var next = summaries[i + 1];

                        summary.scrollToFixed({
                            marginTop: $('.header-two').outerHeight(true) + 10,
                            zIndex: 999
                        });
                    });*/
        });
    </script>
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="<?php echo base_url();?>js/move-top.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({easingType: 'easeOutQuart'});

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->


</head>
<body>


<datalist id="anything">
    <option value="Hotels">Hotels</option>
    <option value="Restarants">Restarants</option>
    <option value="Autocare">AutoCare</option>
</datalist>

<datalist id="cities">
    <option value="Cebu">Cebu</option>
    <option value="Manila">Manila</option>
</datalist>

<datalist id="areas">
    <option value="All">All</option>
    <option value="Soriano Avenue">Soriano Avenue</option>
    <option value="J. de Veyra Street">J. de Veyra Street</option>
</datalist>



<div class="n-ele-center n-white">
    <!-- header -->

    <div class="header">
        <div class="header-two"><!-- header-two -->
            <div class="container">
                <div class="header-logo">
                    <h1><a href="<?php echo site_url();?>"><img class="logo" src="<?php echo base_url();?>images/logo/1.png" alt="quickfinder logo"/></a></h1>
                </div>
                <div class="header-search">
					<form action="<?php echo site_url('Search')?>" method="post"/>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <input type='search'
                                   placeholder='Search anything in Philippines'
                                   class='productsearch'
                                   data-min-length='0'
                                   list='anything'
                                   name='search' />
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   class='citysearch'
                                   data-min-length='0'
                                   list='cities'
                                   placeholder='Select city'
                                   name='flexdatalist-city'
                                   value="Cebu">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   placeholder='Select Area'
                                   class='areasearch'
                                   data-min-length='0'
                                   list='areas'
                                   name='area'
                                   value="All">
                        </div>
                    </div>
				
                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer searchbtn">
                            <button type="submit" class="btn btn-default" aria-label="Left Align">
                                <i class="fa fa-search" aria-hidden="true"> </i>
                            </button>
                        </div>
					</div>
				</form>

                </div>
            </div>
        </div><!-- //header-two -->
    </div>


    <!-- //header -->
    <!-- banner -->
    <div class="banner pad-tp">
        <div class="carousel carousel-slider center ">
            <a class="carousel-item" href="#one!">
                <img src="<?php echo base_url();?>images/subcategories/autocare/auto.png" alt="banner1">
            </a>

        </div>

        <script>
            $(document).ready(function () {

                $('.carousel.carousel-slider').carousel({
                    fullWidth: true,
                    indicators: false
                });

            });

        </script>
    </div>
    <!-- //banner -->
    <!-- deals -->
    <div class="deals card">
        <div> <!--class="container"> -->
            <div class="">
                <div class="center-sub">
					<?php 
							if(!empty($storelist))
							{
								foreach($storelist as $row){?>
                    <div class="row card">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                           <img src="<?php echo base_url();?>admin/<?php echo $row['firstimage'];?>" class="n-img" alt="store thumbnail">
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <h3 class="vender-name"><?php echo $row['storename'];?></h3>
                            <p class="n-contact"><span class=" n-icon fa-phone fa contact-icon"></span>+91-<?php echo $row['mobile'];?></p>
                            <p class="address"><span class=" n-icon fa-diamond fa "></span>
                                <?php echo $row['address'];?>
                            </p>
                        </div>
                    </div>
				<?php }}else{ ?>	
			
                    <div class="row card">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                           
                        </div>
						<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                          <h5>Not Record Found!..</h5>
                        </div>
                        
                    </div>
				<?php } ?>	
                </div>
            </div>

        </div>
    </div>
    <!-- //deals -->


    <!-- subscribe -->
    <div class="subscribe">
        <div class="">
            <div class="col-md-6 social-icons w3-agile-icons">
                <h4>Keep in touch</h4>
                <ul>
                    <li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
                    <li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
                    <li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
                    <li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
                    <li><a href="#" class="fa fa-rss icon rss"> </a></li>
                </ul>
                <ul class="apps">
                    <li><h4>Download Our app : </h4></li>
                    <li><a href="#" class="fa fa-apple"></a></li>
                    <li><a href="#" class="fa fa-windows"></a></li>
                    <li><a href="#" class="fa fa-android"></a></li>
                </ul>
            </div>
            <div class="col-md-6 subscribe-right">
                <h4>Sign up for email</h4>
                <form action="#" method="post">
                    <input type="text" name="email" placeholder="Enter your Email..." required="">
                    <input type="submit" value="Subscribe">
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //subscribe -->
    <div class="copy-right">
        <div class="">
            <p>© 2018 All rights reserved <a href="http://"> Quickfinder</a></p>
        </div>
    </div>

</div>


<!-- menu js aim -->
<script src="<?php echo base_url();?>js/jquery.menu-aim.js"></script>
<script src="<?php echo base_url();?>js/main.js"></script> <!-- Resource jQuery -->
<!-- //menu js aim -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<!---auto suggest---->
<link href="<?php echo base_url();?>plugins/jquery-flexdatalist/jquery.flexdatalist.min.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url();?>plugins/jquery-flexdatalist/jquery.flexdatalist.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/countrysearch.js" type="text/javascript"></script>


<!--auto adjust height of all cols----->
<script type="text/javascript" src="<?php echo base_url();?>plugins/trunk8/trunk8.js"></script>
<script>
    $(function() {
        $('.address').trunk8();
    });
</script>

</body>
</html>