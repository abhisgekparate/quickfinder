<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quick Finder</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>

    <!--- materialize css --->
    <link href="materialize/dist/css/materializeModified.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>

    <link href="css/menu.css" rel="stylesheet" type="text/css" media="all"/> <!-- menu style -->

    <link href="css/ken-burns.css" rel="stylesheet" type="text/css" media="all"/> <!-- banner slider -->
    <link href="css/animate.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all"> <!-- carousel slider -->


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- //Custom Theme files -->
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet">


    <link href="css/subcategories.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <!-- //js -->

    <!---- materialize js ---->
    <script src="materialize/dist/js/materialize.min.js" type="text/javascript"></script>

    <script src="js/jquery-scrolltofixed-min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            // Dock the header to the top of the window when scrolled past the banner. This is the default behaviour.

            //$('.header-two').scrollToFixed();
            // previous summary up the page.
            /*
                    var summaries = $('.summary');
                    summaries.each(function(i) {
                        var summary = $(summaries[i]);
                        var next = summaries[i + 1];

                        summary.scrollToFixed({
                            marginTop: $('.header-two').outerHeight(true) + 10,
                            zIndex: 999
                        });
                    });*/
        });
    </script>
    <!-- start-smooth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {

            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };

            $().UItoTop({easingType: 'easeOutQuart'});

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->


</head>
<body>
<div class="agileits-modal modal fade" id="myModal88" tabindex="-1" role="dialog" aria-labelledby="myModal88"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-map-marker" aria-hidden="true"></i> Location
                </h4>
            </div>
            <div class="modal-body modal-body-sub">
                <h5>Select your delivery location </h5>
                <select class="form-control bfh-states" data-country="US" data-state="CA">
                    <option value="">Select Your location</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AS">American Samoa</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District of Columbia</option>
                    <option value="FM">Federated States Of Micronesia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="GU">Guam</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PW">Palau</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VI">Virgin Islands</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>
                <input type="text" name="Name" placeholder="Enter your area / Landmark / Pincode" required="">
                <button type="button" class="close2" data-dismiss="modal" aria-hidden="true">Skip & Explore</button>
            </div>
        </div>
    </div>
</div>


<datalist id="anything">
    <option value="Hotels">Hotels</option>
    <option value="Restarants">Restarants</option>
    <option value="Autocare">AutoCare</option>
</datalist>

<datalist id="cities">
    <option value="Cebu">Cebu</option>
    <option value="Manila">Manila</option>
</datalist>

<datalist id="areas">
    <option value="All">All</option>
    <option value="Soriano Avenue">Soriano Avenue</option>
    <option value="J. de Veyra Street">J. de Veyra Street</option>
</datalist>



<div class="n-ele-center n-white">
    <!-- header -->

    <div class="header">
        <div class="header-two"><!-- header-two -->
            <div class="container">
                <div class="header-logo">
                    <h1><a href="index.html"><img class="logo" src="images/logo/1.png" alt="quickfinder logo"/></a></h1>
                </div>
                <div class="header-search">

                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <input type='search'
                                   placeholder='Search anything in Philippines'
                                   class='productsearch'
                                   data-min-length='0'
                                   list='anything'
                                   value='Auto Care'
                                   name='productsearch'>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   class='citysearch'
                                   data-min-length='0'
                                   list='cities'
                                   placeholder='Select city'
                                   name='citysearch'
                                   value="Cebu">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer">
                            <i class="fa fa-map-marker" aria-hidden="true"> </i>
                            <input type='search'
                                   placeholder='Select Area'
                                   class='areasearch'
                                   data-min-length='0'
                                   list='areas'
                                   name='areasearch'
                                   value="All">
                        </div>
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 pd-0">
                        <div class="searchContainer searchbtn">
                            <button type="submit" class="btn btn-default" aria-label="Left Align">
                                <i class="fa fa-search" aria-hidden="true"> </i>
                            </button>
                        </div>
                    </div>


                </div>
            </div>
        </div><!-- //header-two -->
    </div>


    <!-- //header -->
    <!-- banner -->
    <div class="banner pad-tp">
        <div class="carousel carousel-slider center ">
            <a class="carousel-item" href="#one!">
                <img src="images/subcategories/autocare/auto.png" alt="banner1">
            </a>

        </div>

        <script>
            $(document).ready(function () {

                $('.carousel.carousel-slider').carousel({
                    fullWidth: true,
                    indicators: false
                });

            });

        </script>
    </div>
    <!-- //banner -->
    <!-- deals -->
    <div class="deals card">
        <div> <!--class="container"> -->
            <div class="">
                <div class="center-sub">
                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for Auto Care Subcategories.." title="Type in a name">

                    <ul id="myUL">
                        <li><a href="stores.html">Car Repair</a></li>
                        <li><a href="stores.html">Car Wash</a></li>

                        <li><a href="stores.html">MoterCycle Repair</a></li>
                        <li><a href="stores.html">Scooter repair</a></li>

                        <li><a href="stores.html">Car Tyres</a></li>
                        <li><a href="stores.html">Car Batteries</a></li>
                        <li><a href="stores.html">Car </a></li>
                    </ul>

                </div>
            </div>

        </div>
    </div>
    <!-- //deals -->


    <!-- subscribe -->
    <div class="subscribe">
        <div class="">
            <div class="col-md-6 social-icons w3-agile-icons">
                <h4>Keep in touch</h4>
                <ul>
                    <li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
                    <li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
                    <li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
                    <li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
                    <li><a href="#" class="fa fa-rss icon rss"> </a></li>
                </ul>
                <ul class="apps">
                    <li><h4>Download Our app : </h4></li>
                    <li><a href="#" class="fa fa-apple"></a></li>
                    <li><a href="#" class="fa fa-windows"></a></li>
                    <li><a href="#" class="fa fa-android"></a></li>
                </ul>
            </div>
            <div class="col-md-6 subscribe-right">
                <h4>Sign up for email</h4>
                <form action="#" method="post">
                    <input type="text" name="email" placeholder="Enter your Email..." required="">
                    <input type="submit" value="Subscribe">
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //subscribe -->
    <div class="copy-right">
        <div class="">
            <p>© 2018 All rights reserved <a href="http://"> Quickfinder</a></p>
        </div>
    </div>

</div>


<!-- menu js aim -->
<script src="js/jquery.menu-aim.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<!-- //menu js aim -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<!---auto suggest---->
<link href="plugins/jquery-flexdatalist/jquery.flexdatalist.min.css" rel="stylesheet" type="text/css"/>
<script src="plugins/jquery-flexdatalist/jquery.flexdatalist.js" type="text/javascript"></script>
<script src="js/countrysearch.js" type="text/javascript"></script>


<!--auto adjust height of all cols----->
<script type="text/javascript" src="plugins/jquery-match-height/jquery.matchHeight.js"></script>
<script>
    $(function() {
        $('.deals').matchHeight();
    });
</script>

<script>
    function myFunction() {
        var input, filter, ul, li, a, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        ul = document.getElementById("myUL");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";

            }
        }
    }
</script>


</body>
</html>