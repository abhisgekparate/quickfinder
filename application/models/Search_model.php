<?php
class Search_model extends CI_Model {

	public function getStores()
	{
		$this->db->select('stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile,categories.name,payment.payment_type,store_payment.IsActive');
		$this->db->from('stores');
		$this->db->join('categories','categories.id=stores.categoryid');
		$this->db->join('store_payment','store_payment.storeid=stores.id');
		$this->db->join('payment','payment.id=store_payment.paymentid');
		return $this->db->get()->result_array();	
	}
	public function getSearch()
	{
		//print_r($_POST);
		//$_POST['flexdatalist-city'];
		//$_POST['area'];
		//$_POST['flexdatalist-search'];
		
		$search=trim($_POST['flexdatalist-search']);
		$city=$_POST['flexdatalist-city'];
		if($_POST['area']=='All')
		{
			$area='';
		}
		else
		{
			$area=$_POST['area'];
		}
		$searchlen= strlen($search);
		$query=explode(" ",$search);
		$records=array();
			if(!empty($city) && !empty($search) && !empty($area))
			{
				return $this->ifNotNullSearch($city,$query,$area);
			}
			elseif(!empty($city) && !empty($search) && empty($area))
			{			
				return $this->ifAreaNull($city,$query);
			}
			elseif(!empty($city) && empty($search) && !empty($area))
			{			
				return $this->ifSearchNull($city,$area);
			}
			elseif(!empty($city) && empty($search) && empty($area))
			{			
				return $this->ifSearchAreaNull($city);
			}
			
	}
	public function ifSearchNull($city,$area)
	{
		$records=array();
		$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = 'cebu' and `stores`.`area` = '$area'  ";
		$premiumRecords=$this->db->query($sql)->result_array();
		foreach($premiumRecords as $row)
			{
				$records[]=$row;
			}				
		return $records;
	}
	public function ifAreaNull($city,$query)
	{
		$condition='';
		$records=array();
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "stores.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR categories.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				
				$condition = substr($condition, 0, -4);
				$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = 'cebu'  and  ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}				
				return $records;
	}
	public function ifSearchAreaNull($city)
	{
		$records=array();
		$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = 'cebu' ";
		$premiumRecords=$this->db->query($sql)->result_array();
		foreach($premiumRecords as $row)
			{
				$records[]=$row;
			}				
		return $records;
	}
	public function ifNotNullSearch($city,$query,$area)
	{
		$records=array();
		$condition='';
		for($i=0;count($query)>$i;$i++)
				{
					if(trim($query[$i]))
					$condition .= "stores.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR categories.name LIKE '%".mysqli_real_escape_string($this->get_mysqli(),$query[$i])."%'OR  ";
				}
				$condition = substr($condition, 0, -4);
				$sql="SELECT stores.address,stores.id,stores.firstimage,stores.name as storename,stores.email,stores.mobile FROM `stores` JOIN `categories` ON `categories`.`id`=`stores`.`categoryid` JOIN `store_payment` ON `store_payment`.`storeid`=`stores`.`id` WHERE `store_payment`.`IsActive` = 1 AND `stores`.`city` = 'cebu' and `stores`.`area`='$area' and ( $condition)";
				$premiumRecords=$this->db->query($sql)->result_array();
				foreach($premiumRecords as $row)
				{
					$records[]=$row;
				}
				return $records;
	}	
	public function get_mysqli()
	{
		$db = (array)get_instance()->db;
		return mysqli_connect('localhost', $db['username'], $db['password'], $db['database']);
	}
}
?>
	