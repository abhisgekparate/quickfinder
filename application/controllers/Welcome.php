<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Search_model');	
		//$this->load->model('Store_model');	
	}	
	public function index()
	{
		$this->load->view('index');
	}
	public function Search()
	{
		$data['storelist'] = $this->Search_model->getSearch();
        $this->load->view('store', $data);
	}
	
}
